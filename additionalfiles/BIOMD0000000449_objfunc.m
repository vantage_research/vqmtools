function objvalue=BIOMD0000000449_objfunc(variant)
%%
load('C:\VQMtools\files\constants.mat','projectvariables');

configObj=getconfigset(projectvariables.modelobj);
compileOptions=get(configObj,'CompileOptions');

simulationTime=200; % in days, ~ 6months
set(compileOptions,'DimensionalAnalysis',false);
set(configObj,'StopTime',simulationTime);
warning off;
try
    [~,x,~]=sbiosimulate(projectvariables.modelobj,configObj,variant);
    objvalue=sum(x(end,:));
catch 
    objvalue=0;
end

end