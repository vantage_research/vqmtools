Vantage QSP Modelling Tools (VQMTools), are aimed at simplifying engineering tasks encountered during a QSP project. These tools are built on Matlab with a GUI and CLI to use.
For details please refer to the poster https://www.page-meeting.org/default.asp?abstract=9084
VQMTools can be used to

* Run sensitivity analysis of parameters for a given chosen objective function
* Verify if a Virtual Subject satisfies the model constraints
* Estimating parameters to find "Reference virtual subjects"
* Generate a virtual cohort and identify virtual population
* Virtual Population diagnostics
    * Parameter density across population
    * Parameter correlations with in virtual population

Currently the scope is limited to QSP models built as ODEs and read into Simbiology of Matlab(This includes file formats such as SBML).

Wiki Link for gui : https://gitlab.com/vantage_research/vqmtools/wikis/vqmtools-gui

Wiki Link for cli : https://gitlab.com/vantage_research/vqmtools/wikis/vqmtools-cli