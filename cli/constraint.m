% Constraint Class
% Needs Comparands and comparison operator
% Comparand can be
    % Measurement (like time @ <>, value @<>..)
    % constant value (like RT values)
    % string function evaluated @ <>(using current variables and parameters)
        % strfunc is a structure with <params> , <algebric form>
classdef constraint
    properties (GetAccess=public)
        name_;
    end
    properties %(Access=private)
        main_;
        lb_;
        ub_;
        units_={};
        type_='other'; % type_ = 'speciesrange' or 'other'
    end
    methods
        function obj=constraint(varargin) % Constructor with variadic arguments
            % Constructor for 'constraint' class
            % Has the form : a < b < c
            % lb_,main_,ub_ can be either measurement or a numeric value
            % Name-Value arguments :
                % -name : constraint name
                % - a : Can be a 'comparand' object or a numeric object
                % - b : Can be a 'comparand' object or a numeric object
                % - c : Can be a 'comparand' object or a numeric object
                % - type : 'speciesrange' or 'other'
                % - units : a cell array of units of form {'a units',
                % 'b units','c units'}.When there is no a or b
                % pass an empty string
                
            nargs=length(varargin);
            
            names=varargin(1:2:nargs);
            values=varargin(2:2:nargs);            
            
            for inx=1:length(names)
                if strcmp(names{inx},'a')
                    obj.lb_=values{inx};
                elseif strcmp(names{inx},'c')
                    obj.ub_=values{inx};
                elseif strcmp(names{inx},'b')
                    obj.main_=values{inx};
                elseif strcmp(names{inx},'name')
                    obj.name_=values{inx};
                elseif strcmp(names{inx},'units')
                    obj.units_=values{inx};
                elseif strcmp(names{inx},'type')
                    obj.type_=values{inx};
                else
                    assert(false,'Please check your arguments');
                end
            end
            
            if isempty(obj.main_)
                assert(0==1,' please provide b argument');
            elseif isempty(obj.lb_) && isempty(obj.ub_)
                assert(0==1,'Please provide atleast one of a and c arguments');
            end
            
        end
        function boolean_out=verify(this,varargin) 
            % Test if a Virtual Patient has passed this constraint
            % Name-Value arguments
                % - vpsimulation : A table of Simulation dyanamics a virtual patient
                % the first column is for 'time'
            nargs=length(varargin);
            
%             names=varargin(1:2:nargs);
%             values=varargin(2:2:nargs);
            
            vpsimtable=varargin{1};
            if nargs==2
                % varargin{1} would be 'vpsimulation'
                vpsimtable=varargin{2};
            end
            
            response_lb=this.lb_;
            response_ub=this.ub_;
            response_main=this.main_;
            
            if ~isempty(this.lb_) && ~isnumeric(this.lb_)
                response_lb=this.lb_.evaluate(vpsimtable);
            end
            if ~isempty(this.ub_) && ~isnumeric(this.ub_)
                response_ub=this.ub_.evaluate(vpsimtable);
            end
            if ~isnumeric(this.main_)
                response_main=this.main_.evaluate(vpsimtable);
            end
            
            boolean_out=true;
            if ~isempty(this.lb_)
                boolean_out=boolean_out && (response_lb < response_main);
            end
            if ~isempty(this.ub_)
                boolean_out=boolean_out && (response_main < response_ub);
            end
            
        end
        
        function txt=tostring(this)
            % Returns 'constraint' object as a string
            if strcmp(this.type_,'speciesrange')
                % just show lb_ < variablename < ub_
                txt='';
                if ~isempty(this.lb_)
                    txt=[num2str(this.lb_) ' <'];
                end
                txt=[txt this.main_.variablename()];
                if ~isempty(this.ub_)
                    txt=[txt ' <' num2str(this.ub_)];
                end
                txt=strjoin(txt);
            else
                % show 
                % lb_ < measurement < ub_
                txt='';
                if ~isempty(this.lb_)
                    txt=strjoin({show(this.lb_),'<'});
                end
                txt=strjoin({txt,show(this.main_)});
                if ~isempty(this.ub_)
                    txt=strjoin({txt,'<',show(this.ub_)});
                end
                                
            end
        end
        
        function txt=name(this)
            txt=this.name_;
        end
    end
end

function txt=show(i_comparand)
    if isnumeric(i_comparand)
        txt=num2str(i_comparand);
    else
        txt=i_comparand.tostring();
    end
end

