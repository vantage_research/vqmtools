function showprogress(curIteration,maxIterations,msg)
    numbars=50;
    totallength=numbars+2;
    if curIteration==1
        fprintf(1,['\n' msg ' :\n']);
    else
        fprintf(repmat('\b',1,totallength));
    end
    str=repmat('-',1,totallength);
    str(1)='|';str(totallength)='|';
    
    numfill=floor(numbars*curIteration/maxIterations);
    for i=2:1+numfill
        str(i)='#';
    end
    fprintf(1,str);
    if curIteration==maxIterations
        fprintf(1,'\n');
    end
%     pause(0.1);
end