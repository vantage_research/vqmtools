classdef ProgressBar
    properties (Access=private)
        % -1 not yet started
        % 0 started
        % 1 ended
        state=-1;        
        numbars=10;
        msglength=15;
        totallength=27;
%         maxIterations=0;
    end
    methods
%         function obj=ProgressBar()
%             obj.maxIterations=maxIterations;
%         end
        function this=show(this,p,msg)
            if length(msg)>this.msglength
                % truncate text for > msglength characters
                msg=msg(1:this.msglength);
            else
                msg=pad(msg,this.msglength);
            end
            if this.state==-1
                fprintf(1,['\n' msg]);
                this.state=0;
            else
                fprintf(repmat('\b',1,this.totallength));
                fprintf(1,msg);
            end
            str=repmat('-',1,this.numbars+2);
            str(1)='|';str(this.numbars+2)='|';

            numfill=floor(p*this.numbars);
            for i=2:1+numfill
                str(i)='#';
            end
            fprintf(1,str);
        end
        function this=finish(this)
            this.show(1,'Finished');
            this.state=1;
            fprintf(1,'\n');
        end
    end
end