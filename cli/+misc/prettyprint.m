function prettyprint(varargin)
%% prettyprint - returns table,matrices and cell arrays in a readable string format
% Usage :
%   prettyprint(arr)
% Input :
%   - arr: matrix or a cell array
%   - header : Header text
%%
    
    arr=varargin{1};
    if size(arr,2) > size(arr,1)
        arr=arr';
    end
    
    if size(arr,2)==1
        print1dcell(varargin);
    else
        print2dcell(varargin);
    end
end

function print2dcell(varargin)
    load('C:\VQMtools\files\constants.mat','projectvariables');

    varargin=varargin{1};
    arr=varargin{1};
    modifiedcellarr=arr;
    for colinx=1:size(arr,2)
        if islogical(arr{1,colinx})
            for rowinx=1:size(arr,1)
                modifiedcellarr{rowinx,colinx}='PASS';
                if ~arr{rowinx,colinx}
                    modifiedcellarr{rowinx,colinx}='FAIL';
                end
            end
        end
    end
    
        
    %% Body of the text
    if length(varargin)>1
        outtxt=external.table_txt(modifiedcellarr,'headerTxt',varargin{2});
    else
        outtxt=external.table_txt(modifiedcellarr);
    end
    
    temp=strsplit(outtxt,'\n');
    rowlength=length(temp{1});
    
    %% TextHeader : Title and subtitle
    titletxt=projectvariables.modelname;
    subtitletxt=varargin{3};
    
    rowlength=max([rowlength length(titletxt) length(subtitletxt)]);
    % Title
    titlefront=repmat(' ',1,floor(0.5*(rowlength-length(titletxt))));
    title=sprintf('%s%s',titlefront,upper(titletxt));
    remlength=rowlength-length(title);
    titleend=repmat(' ',1,remlength);
    title=sprintf('%s%s',title,titleend);
    
    % Subtile for displayed text (Optional)
    subtitlefront=repmat(' ',1,floor(0.5*(rowlength-length(subtitletxt))));
    subtitle=sprintf('%s%s',subtitlefront,subtitletxt);
    remlength=rowlength-length(subtitle);
    subtitleend=repmat(' ',1,remlength);
    subtitle=sprintf('%s%s',subtitle,subtitleend);
        
    head=sprintf('%s\n%s\n%s\n',title,subtitle,repmat('=',1,rowlength));
    
    %% Display text
    searchtxt='FAIL';
    pos=strfind(outtxt,searchtxt);
    lastpos=1;
    fprintf(head);
    for pinx=1:length(pos)
        fprintf(outtxt(lastpos:pos(pinx)-1));
        fprintf(2,outtxt(pos(pinx):pos(pinx)+length(searchtxt)));
        % NOTE :This can used for 'TRUE'
        % disp(strcat('<a href="">',outtxt(pos(pinx):pos(pinx)+length(searchtxt)),'</a>'));
        lastpos=pos(pinx)+length(searchtxt)+1;
    end
    fprintf(outtxt(lastpos:end));
end

function print1dcell(varargin)
    load('C:\VQMtools\files\constants.mat','projectvariables');
    
    varargin=varargin{1};
    arr=varargin{1};
    a=zeros(length(arr),1);
    for inx=1:length(arr)
        a(inx)=length(arr{inx});
    end
    maxlen=max(a);
    sep='|';fmt=' %s %-*s';
    
    %% Body of text displayed
    txt='';
    rowlength=1;
    for inx=1:3:3*floor(length(arr)/3)
%         row=sprintf('%*s %*s %*s\n',maxlen,arr{inx},maxlen,arr{inx+1},maxlen,arr{inx+2});
        row='';
        row = [row sprintf(fmt,sep,2,num2str(inx))];
        row = [row sprintf(fmt,sep,maxlen,arr{inx})];
        row = [row sprintf(fmt,sep,maxlen,arr{inx+1})];
        row = [row sprintf(fmt,sep,maxlen,arr{inx+2})];
        txt=[txt sprintf('%s\n',row)];
        rowlength=length(row);
    end
    
    inx=inx+3;
    row='';
    row = [row sprintf(fmt,sep,2,num2str(inx))];
    while inx<=length(arr)
        row = [row sprintf(fmt,sep,maxlen,arr{inx})];
        inx=inx+1;
    end
    txt=[txt sprintf('%s\n',row)];
    
    %% TextHeader : Title and subtitle
    if length(varargin)==2
        rowlength=max([rowlength length(projectvariables.modelname) length(varargin{2})]);
    else
        rowlength=max([rowlength length(projectvariables.modelname)]);
    end
    % Title
    titlefront=repmat(' ',1,floor(0.5*(rowlength-length(projectvariables.modelname))));
    title=sprintf('%s%s',titlefront,upper(projectvariables.modelname));
    remlength=rowlength-length(title);
    titleend=repmat(' ',1,remlength);
    title=sprintf('%s%s',title,titleend);
    
    % Subtile for displayed text (Optional)
    subtitle='';
    if length(varargin)==2
        subtitlefront=repmat(' ',1,floor(0.5*(rowlength-length(varargin{2}))));
        subtitle=sprintf('%s%s',subtitlefront,varargin{2});
        remlength=rowlength-length(subtitle);
        subtitleend=repmat(' ',1,remlength);
        subtitle=sprintf('%s%s',subtitle,subtitleend);
    end
%     head=repmat('=',1,rowlength);
    head=sprintf('%s\n%s\n%s',title,subtitle,repmat('=',1,rowlength));
    
    
    %% Append header to the beginning txt
    txt=sprintf('\n %s\n%s',head,txt);

    fprintf(txt);
end