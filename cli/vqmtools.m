function vqmtools()
% Hey there, thanks for trying out VQMTools for your QSP project.
%              _   _  ________  ___  _____           _     
%             | | | ||  _  |  \/  | |_   _|         | |    
%             | | | || | | | .  . |   | | ___   ___ | |___ 
%             | | | || | | | |\/| |   | |/ _ \ / _ \| / __|
%             \ \_/ /\ \/' / |  | |   | | (_) | (_) | \__ \
%              \___/  \_/\_\_|  |_/   \_/\___/ \___/|_|___/
% List of namespaces and functions
%  ->model
%    ->read : read QSP model
%    ->listelements : list elements from QSP model
%    ->verifyvp : verify if a given VP satisfies model constraints
%  ->analysis
%    ->pestimation : estimate parameters to minimize an objective
%    ->lsa : run sensitivity analysis of model for different parameters
end

