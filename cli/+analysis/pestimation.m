function [finalparamtable,minobjvalue]=pestimation(varargin)
%     Estimates the given variable parameters to 'minimize' the objective function. 
%     Creates a parameter dashboard with the estimated parameters on user
%     input.
%     Inputs : Name-Value arguments
%     ->parameterdashboard : filepath to '.csv' file(comma separated) with following columns        
%         ---------------------------------------
%         | name | isvariable | value | lb | ub |
%         ---------------------------------------  
%         |      |            |       |    |    |
%         |      |            |       |    |    |
%         |      |            |       |    |    |            
%         'name' is the column for name of parameter.        
%         'isvariable' is a boolean set to true for a
%         variable parameter or false for a fixed parameter
%         'value' is the value of parameter, incase of variable parameter, this is the
%         initial value.
%         'lb' is lowerbound for the variable parameter
%         'ub' is upperbound for the variable parameter        
%      ->objectivefunction : function handle to minimize
%     Output : 
%     ->finalparamtable : a table with 'name','value' and 'isvariable' columns.
%         'name' column has the name of parameter.        
%         'value' has the parameter value, incase of variable parameter, this is the 
%          final estimated value.        
%         'isvariable' is a boolean set to true for a variable parameter or 
%          false for a fixed parameter
%      ->minobjvalue : minimized objective function value
    %%
    
    if ~(exist('C:\VQMtools\files\initialconditions.csv','file')==2)
        assert(false,'initialconditions.csv file is missing');
        return;
    end

    speciestable_=readtable('C:\VQMtools\files\initialconditions.csv',...
        'Delimiter',',','ReadVariableNames',true,'HeaderLines',0,'Format','%s%f%f%f');
    
    nargs=length(varargin);
    names=varargin(1:2:nargs);
    values=varargin(2:2:nargs);
    
    
    for inx=1:length(names)
        if strcmp(names{inx},'parameterdashboard')
            paramtable_=readtable(values{inx},'Delimiter',',','ReadVariableNames',...
                true,'HeaderLines',0,'Format','%s%f%f%f%f');
        elseif strcmp(names{inx},'objectivefunction')
            objfunc_=values{inx};
        else
            assert(false,'Please check your arguments');
        end
    end
    
    
    %%    
    estimatedparam.inx=find(paramtable_.isvariable==1);
    estimatedparam.lb=paramtable_.lb(estimatedparam.inx);
    estimatedparam.ub=paramtable_.ub(estimatedparam.inx);
    num_estimatedparams=length(estimatedparam.inx);
    
    % Pick best of few random points to run fmincon
    minobjvalue=1e010;

%     wb=waitbar(0,'Running...');
    pbar=misc.ProgressBar();
    pbar=pbar.show(0,'Starting...');
    objvalues=ones(1,50);
    guesses=zeros(num_estimatedparams,50);
    
    for iter=1:50
        x0=estimatedparam.lb +...
            rand(num_estimatedparams,1).*(estimatedparam.ub-estimatedparam.lb);
        
        objvalues(iter)=tempfunc(x0);
        guesses(:,iter)=x0;
        if mod(iter,10)==0
%             waitbar(0.7*iter/50,wb,'Running...');
%             misc.showprogress(iter,50,'Running...');
            pbar=pbar.show(0.7*iter/50,'Running...');
        end
    end
    [~,inx]=min(objvalues);
    bestestimates=guesses(:,inx);
    minobjvalue=objvalues(inx);

    pbar=pbar.show(0.7,'Almost there...');
    options=optimoptions('fmincon','Display','off','UseParallel',false,...
    'MaxIterations',10);
    
    [xoptim,fval,~]=fmincon(@tempfunc,bestestimates,[],[],[],[],...
        estimatedparam.lb,estimatedparam.ub,[],options);

    if fval<minobjvalue
        minobjvalue=fval;
        bestestimates=xoptim;
    end

    pbar.finish();
    
    finalparamtable=table();
    finalparamtable.name=paramtable_.name;
    finalparamtable.isvariable=paramtable_.isvariable;

    finalparamtable.value=paramtable_.value;
    finalparamtable.value(estimatedparam.inx)=bestestimates;
    
    finalparamtable.lb=0.5*finalparamtable.value;
    finalparamtable.ub=2*finalparamtable.value;
    
    txtdata=table2cell(table((1:length(finalparamtable.name))',finalparamtable.name,paramtable_.isvariable,finalparamtable.value,paramtable_.value));
    misc.prettyprint(txtdata,{'Sno','name','isEstimated','Final value','Initial value'},'Parameter Estimation');

    savedashboard=input('Do you want to save dashboard ?? true or false\n');
    if savedashboard
        writetable(finalparamtable,'C:\VQMtools\files\estimatedparameterdashboard.csv',...
            'Delimiter',',');
    end    
    
    
    function output=tempfunc(estimatedparamvalues)
        newptable=paramtable_;
        newptable.value(estimatedparam.inx)=estimatedparamvalues;

        tmp_variant=sbiovariant('tmp_variant');
        for i_inx=1:size(speciestable_,1)
            addcontent(tmp_variant,{'species',speciestable_.name{i_inx},...
                'InitialAmount',speciestable_.value(i_inx)});
        end
        for i_inx=1:size(paramtable_,1)
            addcontent(tmp_variant,{'parameter',newptable.name{i_inx},...
                'Value',newptable.value(i_inx)});
        end

        output=objfunc_(tmp_variant);

    end
    
end

