function lsaoutput=lsa(varargin)
%     Run local sensitivity analysis at value given by parameter dashboard
%     Creates Tornado plot on user input.
%     Inputs : Name-Value arguments
%     ->parameterdashboard: filepath for a '.csv' file with following
%         columns        
%         ---------------------------------------
%         | name | value | isvariable lb | ub |
%         ---------------------------------------
%         |      |       |               |    |
%         |      |       |               |    |
%         |      |       |               |    |
%         name- parameter name
%         value - current parameter value
%         isvariable - if set to 1, sensitivity of model output is checked
%         w.r.t this parameter
%     ->objectivefunction : function handle to objective function
%           Objective function should take 'variant' object as input and
%           return a scalar output

    %%
    load('C:\VQMtools\files\constants.mat','projectvariables');
    
    % Simulate the model at give parameter
    % simulate the model at parameter lower bound
    % simulate the model at parameter upper bound
    
    nargs=length(varargin);
    names=varargin(1:2:nargs);
    values=varargin(2:2:nargs);
    figsavepath_='';
    for inx=1:length(names)
        if strcmp(names{inx},'parameterdashboard')
            paramtable_=readtable(values{inx},'Delimiter',',','ReadVariableNames',true,'HeaderLines',0,'Format','%s%f%f%f%f');
        elseif strcmp(names{inx},'objectivefunction')
            objfunc_=values{inx};
        elseif strcmp(names{inx},'figsavepath')
            figsavepath_=values{inx};
        else
            assert(0==1,'Please check your arguments');
        end
    end
    
    speciestable_=readtable('C:\VQMtools\files\initialconditions.csv',...
        'Delimiter',',','ReadVariableNames',true,'HeaderLines',0,'Format','%s%f%f%f');
    
    % LSA output
    numparameters=size(paramtable_,1);
    lsaoutput=table(cell(numparameters,1),zeros(numparameters,1),zeros(numparameters,1),zeros(numparameters,1),...
        'VariableNames',{'name','outputbaseline','outputlb','outputub'});
    
    baseline_variant=sbiovariant('lsa_variant');
    %% Baseline variant
    warning off;

    for inx=1:size(speciestable_,1)
        addcontent(baseline_variant,{'species',speciestable_.name{inx},'InitialAmount',speciestable_.value(inx)});
    end
    for inx=1:size(paramtable_,1)
        addcontent(baseline_variant,{'parameter',paramtable_.name{inx},'Value',paramtable_.value(inx)});
    end
    
    addvariant(projectvariables.modelobj,baseline_variant);
%     disp(baseline_variant);
    output_at_baseline=objfunc_(baseline_variant);
%     disp(output_at_baseline);
%     assert(1==0,'stopped execution');
    
    lsaoutput.outputbaseline=repmat(output_at_baseline,length(lsaoutput.outputbaseline),1);
    
    %% For each of variable parameter
    % Create a variant with lb and ub for parameter, others at baseline
    % simulate and save the output
    lsaoutputinx=0;
    for inx=1:size(paramtable_,1)
        if paramtable_.isvariable(inx)
            
            minsensitivity=1e10;maxsensitivity=0;
            
            lsaoutputinx=lsaoutputinx+1;            
            
            paramvals=[paramtable_.lb(inx) paramtable_.ub(inx)];
            for paramval=paramvals
                lsaparam_variant=sbiovariant('lsaparam_variant');
                lsaparam_variant.Content=get(baseline_variant,'Content');
                set(lsaparam_variant,'name','lsaparam_variant');

                % replace the baseline value of parameter with its current
                % value
                rmcontent(lsaparam_variant,{'parameter',paramtable_.name{inx},...
                    'Value',paramtable_.value(inx)});
                addcontent(lsaparam_variant,{'parameter',paramtable_.name{inx},...
                    'Value',paramval});

                addvariant(projectvariables.modelobj,lsaparam_variant);            
                curoutput=objfunc_(lsaparam_variant);
                
                minsensitivity=min(minsensitivity,curoutput);
                maxsensitivity=max(maxsensitivity,curoutput);
                
                removevariant(projectvariables.modelobj,'lsaparam_variant');
            end
            
            lsaoutput.name{lsaoutputinx}=paramtable_.name{inx};
            lsaoutput.outputlb(lsaoutputinx)=minsensitivity;
            lsaoutput.outputub(lsaoutputinx)=maxsensitivity;
            
            lsaoutput.lb_sensitivity(lsaoutputinx)=100*(minsensitivity-output_at_baseline)/output_at_baseline;
            lsaoutput.ub_sensitivity(lsaoutputinx)=100*(maxsensitivity-output_at_baseline)/output_at_baseline;
            lsaoutput.sensitivity(lsaoutputinx)=abs(lsaoutput.lb_sensitivity(lsaoutputinx))+abs(lsaoutput.ub_sensitivity(lsaoutputinx));
            
        end
    end
    lsaoutput(lsaoutputinx+1:end,:)=[]; % Remove empty rows
    lsaoutput=sortrows(lsaoutput,'sensitivity','descend');
    
    txtdata=table2cell(table((1:length(lsaoutput.name))',lsaoutput.name,lsaoutput.sensitivity));
    misc.prettyprint(txtdata,{'Sno','name','sensitivity'},'Parameter Sensitivities');

    savetornadoplot=input('Do you want to save as tornado plot ?? true or false\n');
    
    if savetornadoplot
        figsavepath='C:\VQMtools\figures\parametersensitivities.png';
        lsaoutput=sortrows(lsaoutput,'sensitivity','ascend');
        tornado=figure('visible','off');
        barh(lsaoutput.lb_sensitivity,'b');
        hold on
        barh(lsaoutput.ub_sensitivity,'r');
        xlim([-100 100]);
        title(['Parameter Sensitivities for ' func2str(objfunc_)],'Interpreter','none');
        xlabel('% Change from baseline');
        ylabel('Parameters');
        yticks(1:32);
        yticklabels(lsaoutput.name);
        set(gca,'TickLabelInterpreter','none')
        set(gca,'FontSize',14,'FontWeight','bold');
        legend({'low','high'},'Location','southeast');

        saveas(tornado,figsavepath);
    end

end
