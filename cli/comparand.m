classdef comparand
    properties %(Access = private)
        fh_;
        measurementarr_;        
    end
    methods
        function this=comparand(varargin)
%             Constructor for comparand
%             Input : Name-value arguments
%               ->function : function handle
%               ->measurements : an array of measurements in function
              
              nargs=length(varargin);
              names=varargin(1:2:nargs);
              values=varargin(2:2:nargs);
              
              for inx=1:length(names)
                  if strcmp(names{inx},'function')
                      this.fh_=values{inx};                      
                  elseif strcmp(names{inx},'measurements')
                      this.measurementarr_=values{inx};
                  else
                      assert(false,'Please check your arguments');
                  end
              end                           
        end
        
        function value=evaluate(this,vpsim)
            m=zeros(length(this.measurementarr_),1);
            
            for inx=1:length(this.measurementarr_)
                m(inx)=this.measurementarr_(inx).evaluate(vpsim);
            end
            value=this.fh_(m);
        end
        
        function str=tostring(this)
            str=func2str(this.fh_);
            % extract the string of arguments for fh_
            % arguments are between first of '(' at pos=2 until ')'
%             paramarray=extractAfter(strtok(str,')'),2);
%             for inx=1:length(this.measurementarr_)
%                 strline=strjoin({strcat(paramarray,'(',num2str(inx),')'),this.measurementarr_(inx).tostring()},'=');
%                 str=strjoin({str,strline},'\n');
%             end
        end
        function measurementstr=measurementstostring(this)
            str=func2str(this.fh_);
            measurementstr='';
            % extract the string of arguments for fh_
            % arguments are between first of '(' at pos=2 until ')'
            paramarray=extractAfter(strtok(str,')'),2);
            for inx=1:length(this.measurementarr_)
                strline=strjoin({strcat(paramarray,'(',num2str(inx),')'),this.measurementarr_(inx).tostring()},'=');
                measurementstr=strjoin({measurementstr,strline},'\n');
            end
        end
        function str=variablename(this)
            if length(this.measurementarr_)~=1
                str='';
                return;
            end
            
            str=this.measurementarr_(1).variablename();
            
        end
    end
end