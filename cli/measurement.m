classdef measurement
    properties %(Access=private)%TODO
        variablename_='';
        type_='';
        point_;
        variableindex_=0; % optional
    end
    properties (Access=public)
        % These are also 'Constant' but should not be seen
        typesarr={'value at time','time at minimum','time at maximum','value at minimum','value at maximum'};
        functionhandles={@analysis.measurementfunction.valueattime,@temp.timeatminimum,@analysis.measurementfunction.timeatmaximum,@analysis.measurementfunction.valueatminimum,@analysis.measurementfunction.valueatmaximum};
        methodcontainer;
    end
    properties (Constant)
        PRECISION=1e-03;
        TIMEINDEX=1; % In VP simulation, time is at 1st column
    end
    methods
        function obj=measurement(varargin)
            % Constructor for measurement class
            % Name-value Arguments :
                % -variablename_
                % -type_
                % -point_: double, 'end' for endpoint, 'all' for all time
                % points
                % -variableindex_ (optional)
            nargs=length(varargin);
            assert(mod(nargs,2)==0,'Please check your arguments');
            
            names=varargin(1:2:nargs);
            values=varargin(2:2:nargs);
            
            for inx=1:length(names)
                if strcmp(names{inx},'variablename')
                    obj.variablename_=values{inx};
                elseif strcmp(names{inx},'type')
                    obj.type_=values{inx};
                elseif strcmp(names{inx},'point')
                    obj.point_=values{inx};
                elseif strcmp(names{inx},'variableindex')
                    obj.variableindex_=values{inx};
                end
            end
            
            if obj.variableindex_ == 0
                % TODO: Find the variable index
            end
            
            % ContainerMap for calling evaluation functions
            obj.methodcontainer=containers.Map(obj.typesarr,obj.functionhandles);
        end
        function rtout=evaluate(obj,varargin)
            % Evaluate 'measurement' for a virtual patient simulation
            % Name-Value arguments:
                %-vpsimulation : A table of simulated dynamics for a virtual patient
                
            nargs=length(varargin);
            vpsimtable=varargin{1};
            if nargs==2
                % varargin{1} would be 'vpsimulation'
                vpsimtable=varargin{2};
            end
            
            colnames=vpsimtable.Properties.VariableNames;
            inx=1;
            while inx<length(colnames) && ~strcmp(colnames{inx},obj.variablename_)                
                inx=inx+1;
            end
            
            obj.variableindex_=inx;
            
            assert(obj.variableindex_ > 0,'Variable Index not found');

            
            vpsimarr=table2array(vpsimtable);
            fh=obj.methodcontainer(obj.type_);
            rtout=fh(obj,vpsimarr);
        end
        
        function str=tostring(this)
            str=[this.variablename_,':',this.type_];
            if ~isempty(this.point_)
                str=[str,num2str(this.point_)];
            end
        end
        
        function str=variablename(this)
            str=this.variablename_;
        end
        
    end
    

end
    
    