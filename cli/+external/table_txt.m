function txt = table_txt(cell_table,varargin);
%% table_txt - Displays a cell tabular data
%    
%    Cells must contain either a string or text. 
%    
%    Usage:
%        table_text = table_txt(cell_table);
%        table_text = table_txt(...,'PARAM',VAL,...,'FLAG')
%    
%    Inputs:
%        cell_table - Multi-dimensional cell table
%    
%    Options: ('param','val' or 'flag')
%        'separator','separator'
%            ['|'] seperator string
%        'headerRow'
%            If specified, will print the first row as a header. Or use 
%            headerTxt.
%        'headerTxt',{'header 1','header 2',...}
%            Cell array of header text. Must be 1D-like and same number 
%            of columns
%        'rjust'
%            If specified, will right-justify
%       'precision',precision
%            [5] precision spec for numeric data. (uses %g formatting)
%
%   Justin Winokur, 2016-08
   
% Do this first 
if isnumeric(cell_table)
    cell_table = num2cell(cell_table);
end
assert(iscell(cell_table),'must enter a cell array');
assert(length(size(cell_table)) <= 2,'2D (or 1D-like) cell arrays only')

%% Process inputs
% Defaults
headerRow = false;
sep = '|';
rjust = false;
precision = 5;

iv = 1;
while iv <= length(varargin)
    switch lower(varargin{iv})
    case {'sep','separator'}
        val = varargin{iv+1};   iv = iv + 1;
        sep = val;
    case {'headerrow','header','header row','head','header'}
        headerRow = true;
    case {'header_txt','headertxt','header txt','header_text','headertext','header txt'}
        val = varargin{iv+1};   iv = iv + 1;
        headerRow = true;
        assert(iscell(val),'Must specify a cell array') 
        cell_table = [val(:)'; cell_table];
    case {'r_just','rjust'};
        rjust = true;
    case {'l_just','ljust'};
        rjust = false; % Already default
    case {'precision','d','n'}
        val = varargin{iv+1};   iv = iv + 1;
        precision = val;
    otherwise
        error(sprintf('Unrecognized param/flag: %s',varargin{iv}))
    end
    iv = iv + 1;
end

sep = strtrim(sep);
if length(sep) == 0; sep = ' ';end

if rjust
    fmt = ' %s %*s';
else
    fmt = ' %s %-*s';
end


% Convert all items to strings and also keep track of sizes
[nrow,ncol] = size(cell_table);

sizes = zeros(nrow,ncol);

for icol = 1:ncol
    for irow = 1:nrow
        item = cell_table{irow,icol};
        if isnumeric(item)
            item = strtrim(sprintf('%0.*g',precision,item));
            cell_table{irow,icol} = item;
        end
        assert(ischar(item),'Table must be text or numeric')
        
        sizes(irow,icol) = length(item);
    end
end

colSize = max(sizes,[],1);
colSize(colSize == 1) = 2;

txt = '';

for irow = 1:nrow
    row = '';
    for icol = 1:ncol
        item = cell_table{irow,icol};
        row = [row sprintf(fmt,sep,colSize(icol),item)];
    end
    row = [row sprintf(' %s',sep)]; % End seperator
    %row = strtrim(row);
    txt = [txt  sprintf('%s\n',row)];
    if headerRow && irow == 1
        row = '';
        for icol = 1:ncol
            item = repmat('-',1,colSize(icol));
            row = [row sprintf('-%s-%-*s',sep,colSize(icol),item)];
        end
        row = [row sprintf('-%s',sep)]; % End seperator
        if row(1) == '-'
            row(1) = ' ';
        end
        %row = strtrim(row);
        txt = [txt  sprintf('%s\n',row)];
    end
end

return
        
