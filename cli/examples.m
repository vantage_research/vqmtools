addpath('C:\Users\admin\Desktop\vqm-cli\')

help vqmtools
help model.read

%% read model
model.read('filepath','modelfiles/BIOMD0000000449_url.xml',...
    'name','Insulin Signalling model');
help model.listelements
model.listelements('species');
model.listelements('parameters');
constraints_def;
model.listelements('constraints');

%% model analysis
analysis.lsa('parameterdashboard','C:\VQMtools\files\dashboard_ex.csv',...
    'objectivefunction',@BIOMD0000000449_objfunc);

analysis.pestimation('parameterdashboard','C:\VQMtools\files\dashboard_ex.csv',...
    'objectivefunction',@BIOMD0000000449_objfunc);
 
model.verifyvp('C:\VQMtools\files\estimatedparameterdashboard.csv');
