function [paramtable,speciestable]=readvalues()
    load('C:\VQMtools\files\constants.mat','projectvariables');
    
    proj_ModelObj=projectvariables.modelobj;

    mainstr=getequations(proj_ModelObj);
    mainstr_parts=(strsplit(mainstr,'\n\n'))';
   
    
    %% Get model parameters from mainstr_parts
    paramstrinx=-1;
    speciesstrinx=-1;
    for inx=1:length(mainstr_parts)
        title=extractBefore(mainstr_parts{inx},':');
        if strcmp(title,'Parameter Values')
            paramstrinx=inx;
        elseif strcmp(title,'Initial Conditions')
            speciesstrinx=inx;
        end
    end
    
    assert(paramstrinx>0 && speciesstrinx>0,'Please verify your model');
    
    modelparamstr=mainstr_parts{paramstrinx};
    modelspeciesstr=mainstr_parts{speciesstrinx};

    paramlines=(strsplit(modelparamstr,'\n'))';% returning a row vector
    specieslines=(strsplit(modelspeciesstr,'\n'))';
    paramlines=paramlines(2:end);
    specieslines=specieslines(2:end);

%     paramlist=cellfun(@(line) strtok(line,' = '),paramlines,'UniformOutput',false);
%     specieslist=cellfun(@(line) strtok(line,' = '),specieslines,'UniformOutput',false);
    
    paramtable=table();
    paramtable.name=cellfun(@(line) strtok(line,' = '),paramlines,'UniformOutput',false);
    paramtable.value=cellfun(@(line) str2double(extractAfter(line,' = ')),paramlines,'UniformOutput',true);

    speciestable=table();
    speciestable.name=cellfun(@(line) strtok(line,' = '),specieslines,'UniformOutput',false);
    speciestable.value=cellfun(@(line) str2double(extractAfter(line,' = ')),specieslines,'UniformOutput',true);
    
%     disp(size(paramtable));
    
    
    %% remove the parameters that are repeated assignments
    equations.assignmentstr=mainstr_parts{3};
    
    params_toremove=zeros(size(paramtable,1),1);
    numRemoved=0;
    for paramInx=1:size(paramtable,1)
        paramname=paramtable.name{paramInx};
        tmp=strfind(equations.assignmentstr,[paramname ' = ']);
        if ~isempty(tmp)
    %         disp(paramtable.name{paramInx})
            numRemoved=numRemoved+1;
            params_toremove(numRemoved)=paramInx;
        end
    end
    params_toremove(numRemoved+1:end)=[];
%     paramlist(params_toremove)=[];
    paramtable(params_toremove,:)=[];
    
%     disp(size(paramtable));
    
    paramtable=sortrows(paramtable,1);
    speciestable=sortrows(speciestable,1);
    

end
