function verifyvp(dashboardfilepath)
%   Verify if VirtualPatient(VP) passess the model constraints. Constraints
%   are read from files\constants.mat
%   Inputs :
%       - parameterdashboard
%   Output :
%       - An array of logical values for either pass or fail to a
%       constraint
%%

    load('C:\VQMtools\files\constants.mat','projectvariables','constraintgroup');

    paramtable_=readtable(dashboardfilepath,'Delimiter',',',...
        'ReadVariableNames',true,...
        'HeaderLines',0,'Format','%s%f%f%f%f');
    
    speciestable_=readtable('C:\VQMtools\files\initialconditions.csv',...
        'Delimiter',',','ReadVariableNames',true,'HeaderLines',0,'Format','%s%f%f%f');

    tmp_variant=sbiovariant('tmp_variant');
    
    for inx=1:size(speciestable_,1)
        addcontent(tmp_variant,{'species',speciestable_.name{inx},'InitialAmount',speciestable_.value(inx)});
    end
    for inx=1:size(paramtable_,1)
        addcontent(tmp_variant,{'parameter',paramtable_.name{inx},'Value',paramtable_.value(inx)});
    end
    
    
    configObj=getconfigset(projectvariables.modelobj);
    compileOptions=get(configObj,'CompileOptions');

    % TODO : Following wont be necessary. These are set by the user once
    simulationTime=200; % in days, ~ 6months
    set(compileOptions,'DimensionalAnalysis',false);
    set(configObj,'StopTime',simulationTime);

    
    numConstraints=length(constraintgroup);
    out=false(numConstraints,1);
    
    warning off;
    [Time,x,SpeciesNames]=sbiosimulate(projectvariables.modelobj,configObj,tmp_variant);
    SpeciesInfo=array2table([Time,x],'VariableNames',['Time';SpeciesNames]);
    
    for inx=1:numConstraints
        out(inx)=constraintgroup{inx}.verify('vpsimulation',...
            SpeciesInfo);
    end

    cc_txt=cell(numConstraints,3);
    for inx=1:length(cc_txt)
        cc_txt{inx,1}=inx;
        cc_txt{inx,2}=constraintgroup{inx}.name();
    end
    cc_txt(:,3)=num2cell(out);
    
    printcolumns=cell(1,3);
    printcolumns{1}='Sno';
    printcolumns{2}='Constraint name';
    printcolumns{3}='VP';
    misc.prettyprint(cc_txt,printcolumns,'Model verification');       
    
end