function listelements(type)
% Prints model elements
% Input :
%  ->'species','parameters','constraints'
    
%%    
    if strcmp(type,'constraints')
        listconstraints();
    elseif strcmp(type,'parameters')
        listparameters();
    elseif strcmp(type,'species')
        listspecies();
    else
        assert(0==1,'Please check your arguments');
    end
end

function listconstraints()
    load('C:\VQMtools\files\constants.mat','constraintgroup');
    
    if ~exist('constraintgroup','var') || size(constraintgroup,1)==0
%         fprintf(2,'There are no constraints in the model\n');
        return
    end
    
    cc_txt=cell(size(constraintgroup,1),3);
    constraintinx=1;
    line=1;
    while constraintinx <= size(constraintgroup,1)
        constraintstr=strsplit(constraintgroup{constraintinx}.tostring(),'\n');
        
        cc_txt{line,1}=constraintinx;
        cc_txt{line,2}=constraintgroup{constraintinx}.name_;        
        cc_txt{line,3}=constraintstr{1};
        
        for inx=2:length(constraintstr)
            line=line+1;
            cc_txt{line,1}='';
            cc_txt{line,2}='';        
            cc_txt{line,3}=constraintstr{inx};
        end
        
        constraintinx=constraintinx+1;
        line=line+1;
    end
    misc.prettyprint(cc_txt,{'Sno','Name','Constraint'},'Model Constraints');
end

function listparameters()
    load('C:\VQMtools\files\constants.mat','projectvariables');

    if ~exist('projectvariables','var')
        return
    end
    
    
    mainstr=getequations(projectvariables.modelobj);
    strparts=(strsplit(mainstr,'\n\n'))';
    
    parampartinx=1;
    while parampartinx<length(strparts) && ~contains(strparts{parampartinx},'Parameter Values')
        parampartinx=parampartinx+1;
    end
    paramlines=(strsplit(strparts{parampartinx},'\n'))';% returning a row vector
    
    paramlines=paramlines(2:end);
    paramlist=cellfun(@(line) strtok(line,' = '),paramlines,'UniformOutput',false);
    misc.prettyprint(paramlist,'Model Parameters');    
end

function listspecies()
    load('C:\VQMtools\files\constants.mat','projectvariables');

    if ~exist('projectvariables','var')
        return
    end

    mainstr=getequations(projectvariables.modelobj);
    strparts=(strsplit(mainstr,'\n\n'))';
    
    partinx=1;
    while partinx<length(strparts) && ...
            ~contains(strparts{partinx},'Initial Conditions')
        partinx=partinx+1;
    end
    lines=(strsplit(strparts{partinx},'\n'))';% returning a row vector
    lines=lines(2:end);

    list=cellfun(@(line) strtok(line,' = '),lines,'UniformOutput',false);
    misc.prettyprint(list,'Model Species');    
end
 