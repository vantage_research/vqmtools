function read(varargin)
% Extracts the model from the project file. Creates dashboards on user confirmation
% Inputs :Name-Value arguments
% ->filepath : path to .sbproj or .xml file
% ->name :(optional) name of the QSP model

%%
    if ~(exist('C:\VQMtools','file')==7)
        mkdir('C:\VQMtools');
    end
    if ~(exist('C:\VQMtools\files','file')==7)
        mkdir('C:\VQMtools\files');
    end
    if ~(exist('C:\VQMtools\figures','file')==7)
        mkdir('C:\VQMtools\figures');
    end
    
    nargs=length(varargin);
    names=varargin(1:2:nargs);
    values=varargin(2:2:nargs);

    replacedashboards_=true;    
    modelname_='QSP Model';
    for inx=1:length(names)
        if strcmp(names{inx},'filepath')
            modelfilelocation_=values{inx};
        elseif strcmp(names{inx},'name')
            modelname_=values{inx};
        elseif strcmp(names{inx},'replacedashboards')
            replacedashboards_=values{inx};
        end
    end

    [~,~,ext] = fileparts(modelfilelocation_);
    if strcmp(ext,'.sbproj')
        Project = sbioloadproject(modelfilelocation_);
        proj_ModelObj = Project.m1;
    elseif strcmp(ext,'.xml')
        proj_ModelObj=sbmlimport(modelfilelocation_);
    end

    %%
    % check for duplicate Species names in the model which may occurs in presence of 
    % compartment scoping and rename them as 'Species_Compartment'
    
    speciesnames = get(proj_ModelObj.Species, 'Name');
    [~,idxuniq] = unique(speciesnames);
    
    if numel(idxuniq) < numel(speciesnames)
        duplicatenames = speciesnames;
        duplicatenames(idxuniq) = [];
        duplicatenames = unique(duplicatenames);
        dupspecObjs = sbioselect(proj_ModelObj,'Type','Species','Name',duplicatenames);
        arrayfun(@(x) rename(x, [x.Name '_' x.Parent.Name]), dupspecObjs);
    end    
    proj_speIDs = get(proj_ModelObj.Species, 'Name');
    proj_speVals = get(proj_ModelObj.Species, 'InitialAmount');

    paramobjs=sbioselect(proj_ModelObj,'Type','Parameter','ConstantValue',true);
    proj_parIDs=get(paramobjs,'Name');
    proj_parVals=get(paramobjs,'Value');

    %%   
    ptable=table(proj_parIDs,cell2mat(proj_parVals),'VariableNames',{'name','value'});
    ptable=sortrows(ptable,1);
    stable=table(proj_speIDs,cell2mat(proj_speVals),'VariableNames',{'name','value'});
    stable=sortrows(stable,1);
    
    projectvariables.modelobj=proj_ModelObj;
    projectvariables.speciesid=stable.name;
    projectvariables.speciesvalue=stable.value;
    projectvariables.parameterid=ptable.name;
    projectvariables.parametervalue=ptable.value;
    projectvariables.modelname=modelname_;
    
    if exist('C:\VQMtools\files\constants.mat','file')==2
        delete('C:\VQMtools\files\constants.mat');
    end
    save('C:\VQMtools\files\constants.mat','projectvariables');
    %% Create dashboards
    replacedashboards=input('Do you want to replace dashboards ?? true or false\n');
    if ~replacedashboards
        return;
    end
    
    % Parameter dashboard
    ptable.isvariable=false(size(ptable,1),1);
    ptable=movevars(ptable,'value','After','isvariable');
    ptable.lb=0.5*ptable.value;
    ptable.ub=2*ptable.value;

    stable.lb=0.5*stable.value;
    stable.ub=2*stable.value;
    
    writetable(ptable,'C:\VQMtools\files\parameterdashboard.csv',...
        'Delimiter',',');
    
    % Species Initial conditions
    writetable(stable,'C:\VQMtools\files\initialconditions.csv',...
        'Delimiter',',');
    
        
end
