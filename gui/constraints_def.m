%   Constraints for the model

%%
% Steady state range constraints
speciesrangesfile='C:\VQMtools\files\speciesranges.csv';
ranges=readtable(speciesrangesfile,'Delimiter',',','ReadVariableNames',true,'HeaderLines',0,...
    'Format','%s%f%f%f%f');

numspecies=size(ranges,1);% lb and ub
cc=cell(numspecies,1);
for inx=1:numspecies
    m=measurement('variablename',ranges.name(inx),'type','value at time',...
        'point','end');
    b=comparand('function',@(x) x,'measurements',m);
    cc{inx}=constraint('name',strcat(ranges.name{inx},' range'),...
        'a',ranges.lb(inx),'b',b,...
        'c',ranges.ub(inx),'type','speciesrange');
end

%%
m_IRip=measurement('variablename','IRip','type','value at time','point','end');
m_IRS1=measurement('variablename','IRS1','type','value at time','point','end');
m_PKB=measurement('variablename','PKB','type','value at time','point','end');

% A<B<C
c1=comparand('function',@(m) m(1)+m(2),'measurements',[m_IRip m_IRS1 m_PKB]);
c2=comparand('function',@(m) 2*m(3),'measurements',[m_IRip m_IRS1 m_PKB]);
cc{numspecies+1}=constraint('name','(IRip+IRS1)> 2*PKB','a',c2,'b',c1,'type','other');
% cc{end}.tostring()
%%
constraintgroup=cc;
save('C:\VQMtools\files\constants.mat','constraintgroup','-append');

