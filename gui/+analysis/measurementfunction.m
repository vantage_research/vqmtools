classdef measurementfunction    
    methods (Static)
        % These can be put as static methods in other class
        function out=valueattime(obj,vpsim)
            if strcmp(obj.point_,'end')
                out=vpsim(end,obj.variableindex_);
                return;
            elseif strcmp(obj.point_,'all') % returns an array
                out=vpsim(:,obj.variableindex_);
                return;
            end
            
            for timeIndex=1:size(vpsim,1)
                if abs(obj.point_-vpsim(timeIndex,1)) < obj.PRECISION                    
                    out=vpsim(timeIndex,obj.variableindex_);
                    return;
                elseif vpsim(timeIndex,1) > obj.point_
                    delT=vpsim(timeIndex,1)-vpsim(timeIndex-1,1);
                    delY=vpsim(timeIndex,obj.variableindex_)-vpsim(timeIndex-1,obj.variableindex_);
                    out=vpsim(timeIndex-1,obj.variableindex_) + (delY/delT)*(obj.point_-vpsim(timeIndex-1,1));
                    return;
                end
            end
                        
        end
        
        function out=timeatminimum(obj,vpsim)
            indexatminimum=find(min(vpsim(:,obj.variableindex_)));
            out=vpsim(indexatminimum,obj.TIMEINDEX);
        end
        
        function out=timeatmaximum(obj,vpsim)
            indexatmaximum=find(max(vpsim(:,obj.variableindex_)));
            out=vpsim(indexatmaximum,obj.TIMEINDEX);
        end
        
        function out=valueatminimum(obj,vpsim)
            out=min(vpsim(:,obj.variableindex_));
        end
        
        function out=valueatmaximum(obj,vpsim)
            out=max(vpsim(:,obj.variableindex_));
        end
        
    end
end