function out=listelements(type)
%     Prints model elements
%     Input :
%         ->type : 'constraints','parameters' or 'species'
    
%%    
    if strcmp(type,'constraints')
        out=listconstraints();
    elseif strcmp(type,'parameters')
        out=listparameters();
    elseif strcmp(type,'species')
        out=listspecies();
    else
        assert(0==1,'Please check your arguments');
    end
end

function out=listconstraints()
    load('C:\VQMtools\files\constants.mat','constraintgroup');
    
    if ~exist('constraintgroup','var') || size(constraintgroup,1)==0
        out=0;
        return
    end
    
    cc_txt=cell(size(constraintgroup,1),3);
    constraintinx=1;
    line=1;
    while constraintinx <= size(constraintgroup,1)
        constraintstr=strsplit(constraintgroup{constraintinx}.tostring(),'\n');
        
        cc_txt{line,1}=constraintinx;
        cc_txt{line,2}=constraintgroup{constraintinx}.name_;        
        cc_txt{line,3}=constraintstr{1};
        
        for inx=2:length(constraintstr)
            line=line+1;
            cc_txt{line,1}='';
            cc_txt{line,2}='';        
            cc_txt{line,3}=constraintstr{inx};
        end
        
        constraintinx=constraintinx+1;
        line=line+1;
    end
    out=table();
    out.sno=cc_txt(:,1);
    out.name=cc_txt(:,2);
    out.constraint=cc_txt(:,3);
end

function out=listparameters()
    load('C:\VQMtools\files\constants.mat','projectvariables');
    
    % TODO: Use readvalues() function
    mainstr=getequations(projectvariables.modelobj);
    strparts=(strsplit(mainstr,'\n\n'))';
    
    parampartinx=1;
    while parampartinx<length(strparts) && ~contains(strparts{parampartinx},'Parameter Values')
        parampartinx=parampartinx+1;
    end
    paramlines=(strsplit(strparts{parampartinx},'\n'))';% returning a row vector
    paramlines=paramlines(2:end);

    paramlist=cellfun(@(line) strtok(line,' = '),paramlines,'UniformOutput',false);
    out=paramlist;
end

function out=listspecies()
    load('C:\VQMtools\files\constants.mat','projectvariables');

    mainstr=getequations(projectvariables.modelobj);
    strparts=(strsplit(mainstr,'\n\n'))';
    
    partinx=1;
    while partinx<length(strparts) && ...
            ~contains(strparts{partinx},'Initial Conditions')
        partinx=partinx+1;
    end
    lines=(strsplit(strparts{partinx},'\n'))';% returning a row vector
    lines=lines(2:end);

    list=cellfun(@(line) strtok(line,' = '),lines,'UniformOutput',false);
    out=list;
end
 