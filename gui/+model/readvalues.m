function [paramtable,speciestable]=readvalues()
    load('C:\VQMtools\files\constants.mat','projectvariables');
    
    proj_ModelObj=projectvariables.modelobj;

    mainstr=getequations(proj_ModelObj);
    mainstr_parts=(strsplit(mainstr,'\n\n'))';
    
    %TODO, read mainstr_parts titles assign indices
%     ODE_PartIndex=
%     Flux_PartIndex=
%     Assignments_PartIndex=
%     Parameters_PartIndex=
%     Species_PartIndex=

    equations.ODEstr=mainstr_parts{1};
    equations.fluxstr=mainstr_parts{2};
    equations.assignmentstr=mainstr_parts{3};
    equations.assignmentList=strsplit(mainstr_parts{3},'\n');

    % Replace assigment expressions into flux eqs
    for inx=2:length(equations.assignmentList)
        expr=(strsplit(equations.assignmentList{inx},' = '))';
        equations.fluxstr=strrep(equations.fluxstr,expr{1},['(' expr{2} ')']);
        equations.ODEstr=strrep(equations.ODEstr,expr{1},['(' expr{2} ')']);
    end

    % Replace flux eqs into ODE eqs
    FluxExprlist=strsplit(equations.fluxstr,'\n');
    for inx=2:length(FluxExprlist)
        expr=(strsplit(FluxExprlist{inx},' = '))';
        equations.ODEstr=strrep(equations.ODEstr,expr{1},['(' expr{2} ')']);
    end
    
    
    %% Get model parameters from mainstr_parts
    modelparamstr=mainstr_parts{4};
    modelspeciesstr=mainstr_parts{5};

    paramlines=(strsplit(modelparamstr,'\n'))';% returning a row vector
    specieslines=(strsplit(modelspeciesstr,'\n'))';
    paramlines=paramlines(2:end);
    specieslines=specieslines(2:end);

%     paramlist=cellfun(@(line) strtok(line,' = '),paramlines,'UniformOutput',false);
%     specieslist=cellfun(@(line) strtok(line,' = '),specieslines,'UniformOutput',false);
    
    paramtable=table();
    paramtable.name=cellfun(@(line) strtok(line,' = '),paramlines,'UniformOutput',false);
    paramtable.value=cellfun(@(line) str2double(extractAfter(line,' = ')),paramlines,'UniformOutput',true);

    speciestable=table();
    speciestable.name=cellfun(@(line) strtok(line,' = '),specieslines,'UniformOutput',false);
    speciestable.value=cellfun(@(line) str2double(extractAfter(line,' = ')),specieslines,'UniformOutput',true);
    
    %% remove the parameters that are repeated assignments
    params_toremove=zeros(size(paramtable,1),1);
    numRemoved=0;
    for paramInx=1:size(paramtable,1)
        paramname=paramtable.name{paramInx};
        tmp=strfind(equations.assignmentstr,[paramname ' = ']);
        if ~isempty(tmp)
    %         disp(paramtable.name{paramInx})
            numRemoved=numRemoved+1;
            params_toremove(numRemoved)=paramInx;
        end
    end
    params_toremove(numRemoved+1:end)=[];
%     paramlist(params_toremove)=[];
    paramtable(params_toremove,:)=[];
    
    paramtable=sortrows(paramtable,1);
    speciestable=sortrows(speciestable,1);
    

end
