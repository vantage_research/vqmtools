function out=verifyvp(dashboardfilepath)
%   Verify if VirtualPatient(VP) passess the model constraints. Constraints
%   are read from files\constants.mat
%   Inputs :
%       - parameterdashboard
%   Output :
%       - An array of logical values for either pass or fail to a
%       constraint
%%

    load('C:\VQMtools\files\constants.mat','projectvariables','constraintgroup');

    if ~exist('constraintgroup','var') || size(constraintgroup,1)==0
        out=0;
        return
    end

    
    paramtable_=readtable(dashboardfilepath,'Delimiter',',',...
        'ReadVariableNames',true,...
        'HeaderLines',0,'Format','%s%f%f%f%f');
    
    speciestable_=readtable('C:\VQMtools\files\initialconditions.csv',...
        'Delimiter',',','ReadVariableNames',true,'HeaderLines',0,'Format','%s%f%f%f');

    tmp_variant=sbiovariant('tmp_variant');
    
    for inx=1:size(speciestable_,1)
        addcontent(tmp_variant,{'species',speciestable_.name{inx},'InitialAmount',speciestable_.value(inx)});
    end
    for inx=1:size(paramtable_,1)
        addcontent(tmp_variant,{'parameter',paramtable_.name{inx},'Value',paramtable_.value(inx)});
    end
    
    
    configObj=getconfigset(projectvariables.modelobj);
    compileOptions=get(configObj,'CompileOptions');

    % TODO : Following wont be necessary. These are set by the user once
    simulationTime=200; % in days, ~ 6months
    set(compileOptions,'DimensionalAnalysis',false);
    set(configObj,'StopTime',simulationTime);

    
    numConstraints=length(constraintgroup);
    
    warning off;
    [Time,x,SpeciesNames]=sbiosimulate(projectvariables.modelobj,configObj,tmp_variant);
    SpeciesInfo=array2table([Time,x],'VariableNames',['Time';SpeciesNames]);
    
    verificationstatus=false(numConstraints,1);
    constraintname=cell(numConstraints,1);
    
    for inx=1:numConstraints
        verificationstatus(inx)=constraintgroup{inx}.verify('vpsimulation',...
            SpeciesInfo);
        constraintname{inx}=constraintgroup{inx}.name();
    end
    out=table();
    out.name=constraintname;
    out.status=verificationstatus;
end